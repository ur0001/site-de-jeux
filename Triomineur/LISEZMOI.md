
# LISEZMOI - Projet Triomineur


## Naissance du Projet

 - ... :
	Idée de Gameplay
 - Fin Novembre 2021 :
	Intérêt à CSS
 - Fin Janvier 2022 :
	Premier Tuto et Premier CSS/HTML
 - Fin Février 2022 :
	Intérêt à Javascript
	Utilisation de la librairie p5.js grace à Florian -> début du Projet

Le nom "trid" vient de "Triangle" + "Grid" (~= "Grille" en anglais)


## Comment télécharger le Projet

 1) Télécharger le Projet sur Gitlab (lien ici bientôt).
 2) Extraire le fichier (**Clique droit** > **Extraire ici**, par exemple).
 3) Double-cliquer sur le fichier **index.html** suffi normalement pour ouvrir le Projet dans un Navigateur Internet.
 4) Sur Windows, il se peu qu'en téléchargeant, extrayant et/ou double-cliquant sur le fichier **index.html** l'ordinateur demande si on est sûr de vouloir entreprendre l'action. Pour pouvoir utiliser le Projet, cette validation est obligatoire.


## Comment mettre à jour le Projet

Comme le Projet n'est pas disponible sur son propre serveur Internet, et qu'il ne peut pas être mis à jour via un luncher (logiciel lanceur d'application) ou une tool-box, il faut le faire soit même :
 1) Aller sur Gitlab (lien) pour voir si il existe une nouvelle version du Projet.
 2) Si il existe une nouvelle version du projet, veuillez suivre les instructions de la section **Comment télécharger le Projet** (vous pouvez supprimer la version actuelle).



## Comment utiliser le Projet
## Bientôt, PEUT-ÊTRE

Truc qu'il reste/qui restent à faire :
 - /changement de dimensions, taille (nombre de case), seuil, apparence (dif, stroke?, ...), ... -> pas de changaments de dimensions, car inutiles et même chiant, et le canvas ne peut pas changer de taille, ou alors il faut rappeler la fonction qui créer le canvas, mais je ne sais pas si ça marche...
 - /grisaille en fin de jeu sur les cases non trouvées -> en fait non, car nouvelles variables, nouvelles couleurs, ...
 - /Recherche approfondie, intensive, intelligente, spontannée, évidente, ... (-> description qui dépasse l'écran)
 - touches : maintenir + activer/désactiver (presque comme)
 - Mettre des images (au moins pour les règles de calcul)
Améliorer l'interface, responsive, ...
Corrections...
Optimisations :
 - 3 (=3*3) -> lambda expressions, ...
 - autre...
Documentation (~)
Plus tard :
 - Score, tableau des scores, score locaux et globaux...
 - enregistrer chez l'utilisateur les préférences (règle de calcul, taille, ...)
> Passage en TypeScript
Gitlab + Lien depuis LinkedIn (?) + Lien sur CV (+ JavaScript).

 - Raccourci, Touche/Clavier pour générer une nouvelle grille
 - Chronomètre
 - Score, Tableau de Score (meilleurs scores locaux -> sur l'appareil local et pas scores globaux, pas de serveur...) + Bouton/Champ pour entrer son nom en cas de victiore.
 - Indépendance par rapport à la librairie p5.js
 - Hébergement sur un Serveur, disponibilité 24H/24 sur Internet
 - Choisir ses propres raccourcis
 - ID d'Optimisation : n'actualiser le canvas (avec la fonction Draw de p5.js) quand quand qulequ'un clique sur la page (eventlistener) ou quand une nouvelle grille est générée... ???
 - images des règles de calcul 3 ou 12 qui s'affichent au bon moment -> ou l'autre apparait en grisée (avec des toggle boutons pour << 12 OU 3 >>)
 - valider les changement en appuyant sur entrée
 - Optimiser les calculs de placement des textes dans les cases
 - Quand la 1ère case déminée est une mine, il régénère une nouvelle grille et re-révèle une nouvelle fois la case... jusqu'à ce que ce ne soit pas une mine
 - afficher les 12ou3 cases autour au survol d'une case (??? BOF inutile) -> genre elles clignotent légèrement et lentement -> option désactivable
 - Anglais/espagnol, ...
 - indices
 - choix des cases et actions avec les boutons (sans la souris, genre avec un toggle -qui peut être atteind avec les touches-)
 - Pouvoir choisir soit même sa couleur de font, et même celles des cases, le texte, ... (+ enregistrées dans les cookies du navigateur ? + présélections pour les daltoniens)

Consignes :
 - Ici les consignes, mais sous plusieurs colonnes, genre en anglais, français, espagnol, LSF..., ... Voir même sous forme de colonnes à gauche, avec la "grille" à droite... ET des panneaux/volets : 1 panneau/volet pour chaque langue, et avec les drapeaux comme logos...
 - Parler aussi des RdC, et bien préciser ici qu'il s'agit des différentes règles de calculs et en aucun cas de Raie du Cul ou de Rez de Chaussée.


## Contacts

Pour envoyer des questions, idées, suggestions au développeur, vous pouvez le contacter via :
 - E-m@il :     *adrien.thomas.56@gmail.com*
 - LinkedIn :   *https://www.linkedin.com/in/adrien-thomas-35r29*


## Remerciements

À **Florian**, pour ses précieux conseils, son aide, et son soutien moral tout au long du developpement.
