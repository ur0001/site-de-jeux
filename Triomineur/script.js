
let nino = 200;

//le nombre de ligne dans la trid
let taille = 15;
//la hauteur du canvas (l'affichage s'adapte directement normalement) ~Y
let hauteur = 520;
//la largeur du canvas (l'affichage s'adapte directement normalement) ~X
let largeur = 600;

//la moitié de la distance (en pixel) entre les triangles affichés (plus ou moins)
let dif = 0;

//pourcentage de mine
let seuil = 15;

//Numéro de règle : les mines sont détectées dans les 3 ou 12 triangles les plus proches...
let regle = 12; // 3 OU 12

//le tableau contenant la trid courante
let trid;

//affiche le texte (qui donne des infos sur le nombre de mine autour de chaque triangle) sur les triangles de la trid
let texteOK = true;

//détermine si la partie en cours est finie
let enJeu = true;

//le nombre de flag sur le terrain
let nbrDrapeau = 0;
//le nombre de mine sur le terrain
let nbrMine = 0;
//le nombre de cases révélées
let nbrRevealed = 0;
//le nombre de cases au total
let nbrCase = 0;

//couleur des cases non révélées
let couleurUnrevealed = "rgb(50, 50, 50)";
//couleur des cases avec un drapeau
let couleurFlagCase = "rgb(127, 0, 255)";
//couleur des drapeaux
let couleurFlag = "rbg(0, 0, 255)";
//couleur du texte
let couleurTexte = "rbg(255, 255, 255)";

//couleur du texte actif
let couleurActif = "rgb(255, 255, 255)";
//couleur du texte inactif
let couleurInactif = "rgb(150, 150, 150)";

//un conteur quelconque
let quelconque = 0;

let btn1 = document.getElementById("newTrid");
btn1.onclick = () => { nouveauTrid() }; /* ou btn1.onclick = nouveauTrid; */
let btn2 = document.getElementById("otherRule");
btn2.onclick = () => { autreRegle() };
let btn3 = document.getElementById("justColor");
btn3.onclick = () => { metTexte() };
let btn4 = document.getElementById("tailleBouton");
btn4.onclick = () => { changeTaille() };
let btn5 = document.getElementById("seuilBouton");
btn5.onclick = () => { changeSeuil() };


function setup() {
	var myCanvas = createCanvas(largeur, hauteur).parent("jeu");
	myCanvas.parent("jeu");
	nouveauTrid();
}


function produitVectoriel(xA, yA, xR, yR) {
	return xA*yR-xR*yA;
}

function pointNDansTriangleBPR(xN, yN, xB, yB, xP, yP, xR, yR) {
	return (produitVectoriel(xB-xP, yB-yP, xB-xN, yB-yN)*produitVectoriel(xB-xN, yB-yN, xB-xR, yB-yR) >= 0)
		&& (produitVectoriel(xP-xB, yP-yB, xP-xN, yP-yN)*produitVectoriel(xP-xN, yP-yN, xP-xR, yP-yR) >= 0)
		&& (produitVectoriel(xR-xB, yR-yB, xR-xN, yR-yN)*produitVectoriel(xR-xN, yR-yN, xR-xP, yR-yP) >= 0);
}

function metTexte() {
	texteOK = !texteOK;
}

function autreRegle() {
	if (regle == 3) {
		regle = 12;
	} else {
		regle = 3;
	}
	nouveauTrid();
}

function changeTaille() {
	taille = document.getElementById("taille").value;
	document.getElementById("taille").value = "";
	nouveauTrid();
}

function changeSeuil() {
	seuil = document.getElementById("seuil").value;
	document.getElementById("seuil").value = "";
	nouveauTrid();
}

function ilYAUneMine(r, a) {
	if (r >= 0 && r < taille && a >=0 && a < trid[r].length && trid[r][a] == -1) {
		return 1;
	} else {
		return 0;
	}
}

function nbrMines(r, a) {
	if (regle == 3) {
		if ((a % 2) == 0) {
			return ilYAUneMine(r-1, a+1) + ilYAUneMine(r, a-1) + ilYAUneMine(r, a+1);
		} else {
			return ilYAUneMine(r, a-1) + ilYAUneMine(r, a+1) + ilYAUneMine(r+1, a-1);
		}
	} else {
		if ((a % 2) == 0) {
			return	ilYAUneMine(r-1, a-1) + ilYAUneMine(r-1, a) + ilYAUneMine(r-1, a+1) + ilYAUneMine(r-1, a+2) + ilYAUneMine(r-1, a+3) +
					ilYAUneMine(r, a-2) + ilYAUneMine(r, a-1) + ilYAUneMine(r, a+1) + ilYAUneMine(r, a+2) + 
					ilYAUneMine(r+1, a-2) + ilYAUneMine(r+1, a-1) + ilYAUneMine(r+1, a);
		} else {
			return	ilYAUneMine(r-1, a) + ilYAUneMine(r-1, a+1) + ilYAUneMine(r-1, a+2) +
					ilYAUneMine(r, a-2) + ilYAUneMine(r, a-1) + ilYAUneMine(r, a+1) + ilYAUneMine(r, a+2) + 
					ilYAUneMine(r+1, a-3) + ilYAUneMine(r+1, a-2) + ilYAUneMine(r+1, a-1) + ilYAUneMine(r+1, a) + ilYAUneMine(r+1, a+1);
		}
	}
}

function revealCase(r, a) {
	if (r < 0) return;
	if (a < 0) return;
	if (r >= taille) return;
	if (a >= trid[r].length) return;
	trid[r][a].reveal();
}

function revealAroundCase(a, r) {
	if (regle == 3) {
		if ((a % 2) == 0) {
			revealCase(r-1, a+1);
			revealCase(r, a-1);
			revealCase(r, a+1);
		} else {
			revealCase(r, a-1);
			revealCase(r, a+1);
			revealCase(r+1, a-1);
		}
	} else {
		if ((a % 2) == 0) {
			revealCase(r-1, a-1);
			revealCase(r-1, a);
			revealCase(r-1, a+1);
			revealCase(r-1, a+2);
			revealCase(r-1, a+3);
			
			revealCase(r, a-2);
			revealCase(r, a-1);
			revealCase(r, a+1);
			revealCase(r, a+2);
			
			revealCase(r+1, a-2);
			revealCase(r+1, a-1);
			revealCase(r+1, a);
		} else {
			revealCase(r-1, a);
			revealCase(r-1, a+1);
			revealCase(r-1, a+2);
			
			revealCase(r, a-2);
			revealCase(r, a-1);
			revealCase(r, a+1);
			revealCase(r, a+2);
			
			revealCase(r+1, a-3);
			revealCase(r+1, a-2);
			revealCase(r+1, a-1);
			revealCase(r+1, a);
			revealCase(r+1, a+1);
		}
	}
}

function estUnDrapeau(r, a) {
	if (r < 0) return 0;
	if (a < 0) return 0;
	if (r >= taille) return 0;
	if (a >= trid[r].length) return 0;
	if (trid[r][a].isFlag) {
		return 1;
	} else {
		return 0;
	}
}

function nbrDrapeauAutour(a, r) {
	if (regle == 3) {
		if ((a % 2) == 0) {
			return estUnDrapeau(r-1, a+1) + estUnDrapeau(r, a-1) + estUnDrapeau(r, a+1);
		} else {
			return estUnDrapeau(r, a-1) + estUnDrapeau(r, a+1) + estUnDrapeau(r+1, a-1);
		}
	} else {
		if ((a % 2) == 0) {
			return	estUnDrapeau(r-1, a-1) + estUnDrapeau(r-1, a) + estUnDrapeau(r-1, a+1) + estUnDrapeau(r-1, a+2) + estUnDrapeau(r-1, a+3) +
					estUnDrapeau(r, a-2) + estUnDrapeau(r, a-1) + estUnDrapeau(r, a+1) + estUnDrapeau(r, a+2) + 
					estUnDrapeau(r+1, a-2) + estUnDrapeau(r+1, a-1) + estUnDrapeau(r+1, a);
		} else {
			return	estUnDrapeau(r-1, a) + estUnDrapeau(r-1, a+1) + estUnDrapeau(r-1, a+2) +
					estUnDrapeau(r, a-2) + estUnDrapeau(r, a-1) + estUnDrapeau(r, a+1) + estUnDrapeau(r, a+2) + 
					estUnDrapeau(r+1, a-3) + estUnDrapeau(r+1, a-2) + estUnDrapeau(r+1, a-1) + estUnDrapeau(r+1, a) + estUnDrapeau(r+1, a+1);
		}
	}
}

function nouveauTableau(r) {
	let a = 0;
	let t = [];
	while (a < (taille-r)*2-1) {
		t.push(0);
		a++;
	}
	return t;
}

function nouveauTrid() {
	enJeu = true;
	
	nbrDrapeau = 0;
	nbrMine = 0;
	nbrRevealed = 0;
	nbrCase = 0;

	let r = 0;
	trid = [];
	let t;
	while (r < taille) {
		t = nouveauTableau(r);
		trid.push(t);
		r++;
	}
	
	r = 0;
	let a;
	while (r < taille) {
		a = 0;
		while (a < (taille-r)*2-1) {
			nbrCase = nbrCase+1;
			if (random(100) < seuil) {
				trid[r][a] = -1;
				nbrMine = nbrMine+1;
			} else {
				trid[r][a] = 0;
			}
			a++;
		}
		r++;
	}
	
	r = 0;
	a;
	while (r < taille) {
		a = 0;
		while (a < (taille-r)*2-1) {
			if (trid[r][a] != -1) {
				trid[r][a] = nbrMines(r, a);
			}
			a++;
		}
		r++;
	}

	r = 0;
	a;
	while (r < taille) {
		a = 0;
		while (a < (taille-r)*2-1) {
			trid[r][a] = new Triangle(a, r, trid[r][a]);
			a++;
		}
		r++;
	}
}

function donneCouleurCase(bpr) {
	if (regle == 3) {
		switch (bpr) {
			case -1 :
				return 'rgb(0, 0, 0)';
			case 0 :
				return 'rgb(255, 255, 255)';
			case 1 :
				return 'rgb(255, 255, 0)';
			case 2 :
				return 'rgb(255, 127, 0)';
			case 3 :
				return 'rgb(255, 0, 0)';
			default :
				return 'rgb(255, 255, 255)';
		}
	} else {
		switch (bpr) {
			case -1 :
				return 'rgb(0, 0, 0)';
			case 0 :
				return 'rgb(255, 255, 255)';
			case 1 :
				return 'rgb(255, 255, 0)';
			case 2 :
				return 'rgb(255, 230, 0)';
			case 3 :
				return 'rgb(255, 207, 0)';
			case 4 :
				return 'rgb(255, 184, 0)';
			case 5 :
				return 'rgb(255, 161, 0)';
			case 6 :
				return 'rgb(255, 138, 0)';
			case 7 :
				return 'rgb(255, 115, 0)';
			case 8 :
				return 'rgb(255, 92, 0)';
			case 9 :
				return 'rgb(255, 69, 0)';
			case 10 :
				return 'rgb(255, 46, 0)';
			case 11 :
				return 'rgb(255, 23, 0)';
			case 12 :
				return 'rgb(255, 0, 0)';
			default :
				return 'rgb(255, 255, 255)';
		}
	}
}

function donneCouleurTexte(bpr) {
	if (regle == 3) {
		switch (bpr) {
			case -1 :
				return 'rgb(236, 0, 0)';
			case 1 :
				return 'rgb(0, 0, 0)';
			case 2 :
				return 'rgb(0, 0, 127)';
			case 3 :
				return 'rgb(0, 0, 255)';
			default :
				return 'rgb(255, 255, 255)';
		}
	} else {
		switch (bpr) {
			case -1 :
				return 'rgb(236, 0, 0)';
			case 1 :
				return 'rgb(0, 0, 0)';
			case 2 :
				return 'rgb(0, 0, 23)';
			case 3 :
				return 'rgb(0, 0, 46)';
			case 4 :
				return 'rgb(0, 0, 69)';
			case 5 :
				return 'rgb(0, 0, 92)';
			case 6 :
				return 'rgb(0, 0, 115)';
			case 7 :
				return 'rgb(0, 0, 138)';
			case 8 :
				return 'rgb(0, 0, 161)';
			case 9 :
				return 'rgb(0, 0, 184)';
			case 10 :
				return 'rgb(0, 0, 207)';
			case 11 :
				return 'rgb(0, 0, 230)';
			case 12 :
				return 'rgb(0, 0, 255)';
			default :
				return 'rgb(255, 255, 255)';
		}
	}
}

function draw() {
	background(85, 85, 85);
	
	let r = 0;
	let a;
	while (r < trid.length) {
		a = 0;
		while (a < trid[r].length) {
			trid[r][a].draw();
			a++
		}
		r++;
	}

	if (!enJeu) {
		fill(color("rgb(255, 0, 0)"));
		text("C'est Perdu !", 10, hauteur-230);
	} else if (nbrRevealed == nbrCase-nbrMine) {
		fill(color("rgb(255, 255, 0)"));
		text("C'est Gagné !", 10, hauteur-230);
	} else if (nbrRevealed*100/(nbrCase-nbrMine) >= 90) {
		if (quelconque >= 20) {
			quelconque = 0;
		}
		if (quelconque >= 10) {
			fill(color("rgb(255, 215, 0)"));
			text("Courage !", 10, hauteur-230);
			quelconque = quelconque+1;
		} else {
			fill(color("rgb(169, 169, 169)"));
			text("Courage !", 10, hauteur-230);
			quelconque = quelconque+1;
		}
	} else {
		fill(color("rgb(0, 255, 0)"));
		text("Bonne Chance !", 10, hauteur-230);
	}

	fill(color(couleurTexte));
	text("Règle de Calcul :\n\t" + regle + " cases", 10, hauteur-185);
	text("Taille (= Hauteur) :\n\t" + taille + " (" + nbrCase + " cases)", 10, hauteur-145);
	text("Seuil (pourcentage de mine) :\n\t" + seuil + " %", 10, hauteur-105);

	if (nbrDrapeau == nbrMine) {
		fill(color(couleurInactif));
	} else {
		fill(color(couleurActif));
	}
	text("Nbr Drapeau/Nbr Bombe à trouver :\n\t" + nbrDrapeau + "/" + nbrMine + " (" + nbrDrapeau*100/nbrMine + " %)", 10, hauteur-65);

	fill(color(couleurTexte));
	text("Nbr Case révélée/Nbr Case à libérée :\n\t" + nbrRevealed + "/" + (nbrCase-nbrMine) + " (" + nbrRevealed*100/(nbrCase-nbrMine) + " %)", 10, hauteur-25);

	if (key === "f") {
		fill(color(couleurActif));
		text("Pour mettre ou enlever un Drapeau (\"!?\") :\n\tappuyez sur \"F\" et sélectionnez une case.", largeur-250, hauteur-65);
		//fill(color(couleurInactif));
		//text("Pour étendre la Recherche :\n\tappuyez sur \"R\" et sélectionnez une case.", largeur-250, hauteur-25);
	} /*else if (key === "r") {//TODO: enlever ceci ?
		fill(color(couleurInactif));
		text("Pour mettre ou enlever un Drapeau (\"!?\") :\n\tappuyez sur \"F\" et sélectionnez une case.", largeur-250, hauteur-65);
		//fill(color(couleurActif));
		//text("Pour étendre la Recherche :\n\tappuyez sur \"R\" et sélectionnez une case.", largeur-250, hauteur-25);
	} */else {
		fill(color(couleurInactif));
		text("Pour mettre ou enlever un Drapeau (\"!?\") :\n\tappuyez sur \"F\" et sélectionnez une case.", largeur-250, hauteur-65);
		//text("Pour étendre la Recherche :\n\tappuyez sur \"R\" et sélectionnez une case.", largeur-250, hauteur-25);
	}
}

//TODO: Cette fonction est sans doute optimisable, mais JE VEUX JOUER !
function mouseClicked() {
	if (!enJeu) return;

	let localY = Math.floor(mouseY*taille/hauteur);

	if (localY < 0) return;
	if (localY >= taille) return;

	let a = 0;
	while (a < trid[localY].length) {
		trid[localY][a].mouseSearchClick(mouseX, mouseY);
		/*if (key === "r") {
			//Révèle toutes les cases restantes autour d'une case révélée et qui est entourée d'autant de drapeau qu'elle n'indique de minde autour d'elle.
			trid[localY][a].mouseSearchClick(mouseX, mouseY);
		} else*/ if (key === "f") {
			trid[localY][a].mouseFlagClick(mouseX, mouseY);
		} else {
			trid[localY][a].mouseRevealClick(mouseX, mouseY);
		}
		a++
	}
}

function fin() {
	let r = 0;
	let a;
	while (r < trid.length) {
		a = 0;
		while (a < trid[r].length) {
			trid[r][a].revealFin();
			a++
		}
		r++;
	}
	nbrRevealed = nbrCase-nbrMine;
}

class Triangle {
	constructor(x, y, bombState) {
		this.x = x;
		this.y = y;
		
		if (this.x%2 == 0) {
			this.xB = this.y*(largeur/taille/2)+(this.x/2)*(largeur/taille)+dif;
			this.yB = this.y*(hauteur/taille)+dif;
			this.xP = this.y*(largeur/taille/2)+((this.x/2)+1)*(largeur/taille)-dif;
			this.yP = this.y*(hauteur/taille)+dif;
			this.xR = this.y*(largeur/taille/2)+(this.x/2)*(largeur/taille)+(largeur/taille/2);
			this.yR = (this.y+1)*(hauteur/taille)-dif;
		} else {
			this.xB = this.y*(largeur/taille/2)+(((this.x-1)/2)+1)*(largeur/taille);
			this.yB = this.y*(hauteur/taille)+dif;
			this.xP = this.y*(largeur/taille/2)+((this.x/2)+1)*(largeur/taille)-dif;
			this.yP = (this.y+1)*(hauteur/taille)-dif;
			this.xR = this.y*(largeur/taille/2)+((this.x-1)/2)*(largeur/taille)+(largeur/taille/2)+dif;
			this.yR = (this.y+1)*(hauteur/taille)-dif;
		}

		this.isBomb = (bombState == -1);
		this.nbrBomb = bombState;
		
		this.isFlag = false;
		this.isRevealed = false;
	}

	draw() {
		if (this.isFlag) {
			fill(color(couleurFlagCase));
			//TODO: noStroke();
			triangle(this.xB, this.yB, this.xP, this.yP, this.xR, this.yR);
			
			fill(color(couleurFlag));
			if (texteOK) {
				text("!?", this.y*(largeur/taille/2)+(this.x/2)*(largeur/taille)+(largeur/taille/2.5), (this.y+0.7)*(hauteur/taille));
			}
		} else if (!this.isRevealed) {
			fill(color(couleurUnrevealed));
			//TODO: noStroke();
			triangle(this.xB, this.yB, this.xP, this.yP, this.xR, this.yR);
		} else {
			fill(color(donneCouleurCase(this.nbrBomb)));
			//TODO: noStroke();
			triangle(this.xB, this.yB, this.xP, this.yP, this.xR, this.yR);
			
			if (texteOK && this.nbrBomb != 0) {
				fill(color(donneCouleurTexte(this.nbrBomb)))
				if (this.nbrBomb != -1) {
					text(this.nbrBomb, this.y*(largeur/taille/2)+(this.x/2)*(largeur/taille)+(largeur/taille/2.4), (this.y+0.7)*(hauteur/taille));
				} else {
					text("*", this.y*(largeur/taille/2)+(this.x/2)*(largeur/taille)+(largeur/taille/2.3), (this.y+0.7)*(hauteur/taille));
				}
			}
		}
	}

	editFlag() {
		if (this.isRevealed) return;

		if (this.isFlag) {
			this.isFlag = false;
			nbrDrapeau--;
		} else {
			this.isFlag = true;
			nbrDrapeau++;
		}
	}

	reveal() {
		if (this.isFlag) return;
		if (this.isRevealed) return;

		this.isRevealed = true;
		nbrRevealed = nbrRevealed+1;
		if (this.nbrBomb == 0) {
			revealAroundCase(this.x, this.y);
		}
		if (this.isBomb && enJeu) {
			enJeu = false;
			fin();
		}
	}

	revealFin() {
		if (this.isFlag) return;
		if (!this.isBomb) return;

		this.isRevealed = true;
	}

	rechercheApprofondie() {
		if (!this.isRevealed) return;

		if (this.nbrBomb <= nbrDrapeauAutour(this.x, this.y)) {
			revealAroundCase(this.x, this.y);
		}
	}

	//TODO: Les 3 fonctions suivantes peuvent être fusionnées ?
	//Fonction pas très précise.
	mouseRevealClick(x, y) {
		if (this.isRevealed) return;
		if (!pointNDansTriangleBPR(x, y, this.xB, this.yB, this.xP, this.yP, this.xR, this.yR)) return;

		this.reveal();
	}
	//Fonction pas très précise.
	mouseFlagClick(x, y) {
		if (this.isRevealed) return;
		if (!pointNDansTriangleBPR(x, y, this.xB, this.yB, this.xP, this.yP, this.xR, this.yR)) return;

		this.editFlag();
	}
	//Fonction pas très précise.
	mouseSearchClick(x, y) {
		if (!this.isRevealed) return;
		if (!pointNDansTriangleBPR(x, y, this.xB, this.yB, this.xP, this.yP, this.xR, this.yR)) return;

		this.rechercheApprofondie();
	}
}
